import {Component, OnInit} from '@angular/core';
import {PlacesService} from './services/places.service';
import {Restaurant} from './models/Restaurant';
import {MapsAPILoader} from '@agm/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  dataLoaded = false;
  tartuBusStation = {
    lat: 58.3799392233872,
    lng: 26.731019795554595
  };
  circleRadius = 1000;
  restaurants: Restaurant[];
  filteredRestaurants: Restaurant[];


  constructor(private placesService: PlacesService,
              private mapsAPILoader: MapsAPILoader) {
  }

  ngOnInit(): void {
    this.placesService.getPlaces(this.tartuBusStation.lat, this.tartuBusStation.lng, 4000, 'burger')
      .subscribe(data => {
        this.restaurants = data.results;
        this.dataLoaded = true;
      });

    this.mapsAPILoader.load().then(() => {
      const center = new google.maps.LatLng(this.tartuBusStation.lat, this.tartuBusStation.lng);
      // markers located outside 1 km distance from center are included
      if (this.restaurants) {
        this.filteredRestaurants = this.restaurants.filter(r => {
          const markerLoc = new google.maps.LatLng(r.geometry.location.lat, r.geometry.location.lng);
          const distanceInKm = google.maps.geometry.spherical.computeDistanceBetween(markerLoc, center) / 1000;
          if (distanceInKm > 1.0) {
            return r;
          }
        });
      }
    });
  }
}
