import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import { AppComponent } from './app.component';
import {AgmCoreModule} from '@agm/core';
import {CustomConstants} from './utils/CustomConstants';
import {HttpClientModule} from '@angular/common/http';
import { PlaceImagesComponent } from './components/place-images/place-images.component';

@NgModule({
  declarations: [
    AppComponent,
    PlaceImagesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: CustomConstants.GOOGLE_API_KEY,
      libraries: ['geometry']
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule { }
