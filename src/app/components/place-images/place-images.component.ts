import {Component, Input, OnInit} from '@angular/core';
import {Restaurant} from '../../models/Restaurant';
import {PlacesService} from '../../services/places.service';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-place-images',
  templateUrl: './place-images.component.html',
  styleUrls: ['./place-images.component.css']
})
export class PlaceImagesComponent implements OnInit {

  @Input() filteredRestaurants: Restaurant[];

  imagesToShow: Array<SafeUrl> = new Array<SafeUrl>();

  constructor(private placesService: PlacesService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.filteredRestaurants.forEach(res => {
      if (res.photos && res.photos[0].photo_reference) {
        this.placesService.getImageByPhotoReference(res.photos[0].photo_reference)
          .subscribe(data => {
            const objectURL = URL.createObjectURL(data);
            this.sanitizer.bypassSecurityTrustUrl(objectURL);
            this.imagesToShow.push(this.sanitizer.bypassSecurityTrustUrl(objectURL));
          });
      }
    });
  }
}
