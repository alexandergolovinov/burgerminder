import {Geometry} from './Geometry';
import {Photo} from './Photo';

export interface Restaurant {
  geometry: Geometry;
  photos: Photo[];
  name: string;
  rating: number;
}

