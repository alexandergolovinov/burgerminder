import {Location} from './Location';

export interface Geometry {
  location: Location;
}
