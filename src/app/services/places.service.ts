import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CustomConstants} from '../utils/CustomConstants';
import {Restaurant} from '../models/Restaurant';

const API_URL = 'https://maps.googleapis.com/maps/api/';

@Injectable({
  providedIn: 'root'
})
/**
 * Service to retrive data from Google Places API
 */
export class PlacesService {

  constructor(private http: HttpClient) {
  }

  /**
   * Method is used to get nearby places from focus point.
   *
   * @param lat - focus point latitude
   * @param lng - focus point longitude
   * @param radius - radius to fetch data
   * @param keyword - place name contains
   */
  getPlaces(lat: number, lng: number, radius: number, keyword: string): Observable<any> {
    const headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origin', '*');
    return this.http.get(API_URL + 'place/nearbysearch/json?location=' +
      lat + ',' + lng +
      '&radius=' + radius +
      '&type=restaurant&' +
      'keyword=' + keyword +
      '&key=' + CustomConstants.GOOGLE_API_KEY, {headers});
  }

  /**
   * Method is used to get image to the place. Blob format is returned.
   *
   * @param photoReference - each Google Place API has photoReference attribute to get images to place.
   */
  getImageByPhotoReference(photoReference: string): Observable<Blob> {
    return this.http.get(API_URL + 'place/photo?maxwidth=300&maxheight=300&photoreference=' +
      photoReference +
      '&key=' + CustomConstants.GOOGLE_API_KEY, {responseType: 'blob'});
  }
}
